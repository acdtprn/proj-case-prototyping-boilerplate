var secrets = require('../config/secrets');
var User = require('../models/User');
var querystring = require('querystring');
var validator = require('validator');
var async = require('async');
var request = require('request');
var _ = require('lodash');
var neo4j = require('node-neo4j');
var neoDb = new neo4j('http://localhost:7474');

/**
 * GET /api/buildings
 * Get all nodes with label "Building"
 */

exports.getBuildings = function(req, res) {
  neoDb.readNodesWithLabel('Building', function(err, result){
    if(err) { res.send(err); }
    (result == false) ? res.sendStatus(400) : res.send(result);
  });
};

/**
 * GET /api/building/:id
 * Get specific node with label "Building" and id = :id
 */

exports.getBuildingById = function(req, res) {
  var paramId=parseInt(req.params.id);
  if(paramId !== undefined) {
    neoDb.cypherQuery("MATCH (n:Building {id:{buildingId}}) RETURN n", { buildingId: paramId }, function(err, result){
      if(err) { console.log(err); }
      (result == false) ? res.sendStatus(400) : res.send(result.data);
    });
  } else {
    //res.sendStatus(300);
  }
};
